module.exports = {
  rules: {
    'vue/valid-v-slot': [
      'error',
      {
        allowModifiers: true,
      },
    ],
    'vue/no-unused-vars': 'off',
    'vue/multi-word-component-names': 0,
    'vue/no-multiple-template-root': 0,
    'space-before-function-paren': 'off',
    'vue/no-v-model-argument': 'off',
    'vue/no-v-html': 'off',
  },
}
