import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
const path = require('path')

export default defineConfig({
  dirs: ['src/components'],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '/src'),
      '~@': path.resolve(__dirname, '/src'),
    },
    dedupe: ['vue'],
  },
  plugins: [
    vue(),
  ],
})
